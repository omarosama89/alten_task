require 'rails_helper'

RSpec.describe CustomersController, type: :controller do
  describe "GET #index" do
    before do
      get :index
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:customer) { FactoryBot.create(:customer) }

    def show
      get :show, params: {id: customer.id}
    end

    it "returns http success" do
      show
      expect(response).to have_http_status(:success)
    end
    it "respond body JSON with attributes" do
      show
      hash_body = JSON.parse(response.body)
      expect(hash_body['customer'].keys).to match_array(["id", "first_name", "last_name", "address", "created_at", "updated_at"])
    end
  end

  describe "POST #create" do
    let(:customer_params) do
      {
          customer: {
              first_name: Faker::Name.first_name,
              last_name: Faker::Name.last_name,
              address: Faker::Address.full_address
          }
      }
    end
    it "creates a new customer" do
      expect { post :create, params: customer_params }.to change(Customer, :count).by(+1)
      expect(response).to have_http_status :created
    end
  end
end
