require 'rails_helper'

RSpec.describe VehiclesController, type: :controller do
  describe "GET #index" do
    before do
      get :index
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:vehicle) { FactoryBot.create(:vehicle) }

    def show
      get :show, params: {id: vehicle.id}
    end

    it "returns http success" do
      show
      expect(response).to have_http_status(:success)
    end
    it "respond body JSON with attributes" do
      show
      hash_body = JSON.parse(response.body)
      expect(hash_body['vehicle'].keys).to match_array(["id", "vehicle_id", "reg_num", "status", "customer_id", "created_at", "updated_at"])
    end
  end

  describe "POST #create" do
    let(:customer) { FactoryBot.create(:customer) }
    let(:vehicle_params) do
      {
          vehicle: {
              vehicle_id: Faker::Vehicle.vin,
              reg_num: Faker::Vehicle.singapore_license_plate,
              status: Vehicle::STATUSES[rand(Vehicle::STATUSES.length)],
              customer_id: customer.id
          }
      }
    end
    it "creates a new vehicle" do
      expect { post :create, params: vehicle_params }.to change(Customer, :count).by(+1)
      expect(response).to have_http_status :created
    end
  end
end
