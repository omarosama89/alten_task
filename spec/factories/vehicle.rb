FactoryBot.define do
  factory :vehicle do
    vehicle_id { Faker::Vehicle.vin }
    reg_num  { Faker::Vehicle.singapore_license_plate }
    customer { create(:customer) }
    status {Vehicle::STATUSES[rand(Vehicle::STATUSES.length)]}
  end
end