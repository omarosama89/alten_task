require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  it "is valid with vehciel_id, reg_num and customer" do
    expect(FactoryBot.build(:vehicle)).to be_valid
  end
  it "is not valid without a vehicle_id" do
    expect(FactoryBot.build(:vehicle, vehicle_id: nil)).to be_invalid
  end
  it "is not valid without a reg_num" do
    expect(FactoryBot.build(:vehicle, reg_num: nil)).to be_invalid
  end
  it "is not valid without a customer" do
    expect(FactoryBot.build(:vehicle, customer: nil)).to be_invalid
  end
  it "is not valid if status is not connected either disconnected" do
    expect(FactoryBot.build(:vehicle, status: 'active')).to be_invalid
  end
end