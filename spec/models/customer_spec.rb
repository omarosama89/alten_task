require 'rails_helper'

RSpec.describe Customer, type: :model do
  it "is valid with first_name, last_name and address" do
    expect(FactoryBot.build(:customer)).to be_valid
  end
  it "is not valid without a first_name" do
    expect(FactoryBot.build(:customer, first_name: nil)).to be_invalid
  end
  it "is not valid without a last_name" do
    expect(FactoryBot.build(:customer, last_name: nil)).to be_invalid
  end

  it "is not valid without an address" do
    expect(FactoryBot.build(:customer, address: nil)).to be_invalid
  end
end