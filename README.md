# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
    * ruby 2.6.0

* System dependencies
    * `ubuntu 18.04`
    * `postgesql` database [Install from here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04)
    * `redis`

* Configuration
    * In order to get started with the application, follow the instructions:- 
        1. open terminal.
        2. navigate to the application directory.
        3. run `bundle install`. This commad will install all the gems used in this application even the `rails`
           gem.
        4. In order to start the server locally, run `rails s`

* Database creation
    * In order to create the database, run `rake db:create`

* Database initialization
    * In order to migrate the database, run `rake db:migrate` 
    * In order to initialize the database with some records, run `rake db:seed`

* How to run the test suite
    * In order to run the test suit, run `rspec`

* Deployment instructions
    * `Heruko` was used in order to deploy app.
