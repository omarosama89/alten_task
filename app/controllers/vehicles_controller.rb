class VehiclesController < ApplicationController
  before_action :set_vehicle, only: [:show, :update, :destroy]

  # GET /vehicles
  def index
    @vehicles = Vehicle.all.order(:id)
    queryParams = params[:queryParams]
    if queryParams.present?
      if queryParams[:customer_id].present?
        @vehicles = @vehicles.where(customer_id: queryParams[:customer_id])
      end
      if queryParams[:status].present?
        @vehicles = @vehicles.where(status: queryParams[:status])
      end
    end

    render json: {vehicles: @vehicles}, status: :ok
  end

  # GET /vehicles/1
  def show
    render json: {vehicle: @vehicle}, status: :ok
  end

  # POST /vehicles
  def create
    @vehicle = Vehicle.new(vehicle_params)

    if @vehicle.save
      render json: {vehicle: @vehicle}, status: :created
    else
      render json: @vehicle.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vehicles/1
  def update
    if @vehicle.update(vehicle_params)
      render json: {vehicle: @vehicle}, status: :ok
    else
      render json: @vehicle.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vehicles/1
  def destroy
    @vehicle.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vehicle_params
      params.require(:vehicle).permit(:vehicle_id, :reg_num, :customer_id)
    end
end
