class VehicleNotifierChannel < ApplicationCable::Channel
  def subscribed
    stream_from "vehicle_notifier"
  end

  def unsubscribed
  end
end
