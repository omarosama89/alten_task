class Customer < ApplicationRecord
  has_many :vehicles

  validates :first_name, :last_name, :address, presence: true
end
