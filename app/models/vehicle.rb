class Vehicle < ApplicationRecord
  include Notifyable
  STATUSES = %w(connected disconnected)
  belongs_to :customer

  attribute :status, :string, default: 'disconnected'

  validates :vehicle_id, :reg_num, presence: true
  validates :status, inclusion: {in: STATUSES}

  after_update :notify_client, if: ->{ self.saved_change_to_attribute?(:status)}
  after_save :set_disconnected_after_1_minute, if: ->{Rails.env == 'development' && self.saved_change_to_attribute?(:status) && self.status == 'connected'}

  def notify_client
    self.notify(vehicle: self.to_json)
  end

  def set_disconnected_after_1_minute
    self.update(status: 'disconnected') unless self.updated_in_last_1_minute?
  end
  if Rails.env == 'development'
    handle_asynchronously :set_disconnected_after_1_minute, run_at: Proc.new { 1.minute.from_now }
  end

  def toggle_status!
    status = self.status
    new_status = status == 'disconnected' ? 'connected' : 'disconnected'
    self.update(status: new_status)
  end

  private
  def updated_in_last_1_minute?
    self.reload
    (DateTime.now.to_i - self.updated_at.to_i) <= 60
  end
end
