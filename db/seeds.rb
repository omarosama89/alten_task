# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


customer = Customer.create(first_name: 'Kalles', last_name: 'Grustransporter', address: 'Cementvägen 8, 111 11 Södertälje')
customer.vehicles.create(vehicle_id: 'YS2R4X20005399401', reg_num: 'ABC123', status: 'connected')
customer.vehicles.create(vehicle_id: 'VLUR4X20009093588', reg_num: 'DEF456', status: 'disconnected')
customer.vehicles.create(vehicle_id: 'VLUR4X20009048066', reg_num: 'GHI789', status: 'connected')

customer = Customer.create(first_name: 'Johans', last_name: 'Bulk', address: 'Balkvägen 12, 222 22 Stockholm')
customer.vehicles.create(vehicle_id: 'YS2R4X20005388011', reg_num: 'JKL012', status: 'connected')
customer.vehicles.create(vehicle_id: 'YS2R4X20005387949', reg_num: 'MNO345', status: 'disconnected')

customer = Customer.create(first_name: 'Haralds', last_name: 'Värdetransporter', address: 'Budgetvägen 1, 333 33 Uppsala')
customer.vehicles.create(vehicle_id: 'VLUR4X20009048066', reg_num: 'PQR678', status: 'connected')
customer.vehicles.create(vehicle_id: 'YS2R4X20005387055', reg_num: 'STU901', status: 'disconnected')
