class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.string :vehicle_id
      t.string :reg_num
      t.belongs_to :customer, foreign_key: true

      t.timestamps
    end
  end
end
