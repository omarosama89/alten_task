class AddStatusToVehicle < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :status, :string
  end
end
