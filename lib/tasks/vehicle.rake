namespace :vehicle do
  desc "TODO"
  task randomize_status: :environment do
    vehicles = Vehicle.all.sample(Vehicle.count/2)
    vehicles.map(&:toggle_status!)
  end
end
