require 'uri'
require 'net/http'

module Notifyable

    def notify(body)
        p 'I am notifying'
        uri = URI(url)  
        req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
        req.body = body.to_json
        use_ssl = Rails.env == 'production' ? true : false
        res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: use_ssl) do |http|
            http.request(req)
        end
        # render json: {success: true, message: 'Message was successfully sent'}, status: :ok
    end

    private
    def url
        "#{CONFIG_ENV['realtime_url']}#{CONFIG_ENV['notify_url']}"
    end
end